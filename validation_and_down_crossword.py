def validation_down_crossword(row: int, column: int, word: str) -> bool:
    if len(word) < 2 or column >= 10 or row >= 10:
        return False
    if len(word) + row > 10:
        return False
    return True



def down_crossword(row: int, column: int, word: str):  
    row -= 1   
    column -= 1
    index = 0
    res = validation_down_crossword(row, column, word)
    if res == False:
        return False
    if res == True:
        for i in range(row, len(word) + row):
            if arr_cross[i][column] == word[index] or arr_cross[i][column] == "*":
                index += 1
            else:
                return "impossible"
            
    new_index = 0
    for i in range(row, len(word) + row):
        arr_cross[i][column] = word[new_index]  
        new_index += 1
    return True
