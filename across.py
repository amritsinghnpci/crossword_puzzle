def impossible_condition(row: int,column: int,word: str):
    if len(word) < 2 or column >= 10 or row >= 10:
        return "impossible"
def invalid_condition(word: str,column: int):
    if len(word) + column > 10:
        return False
    else:
        return True

def across_crossword(row: int,column: int,word: str):
    row -= 1
    column -= 1
    index = 0
    impossible_condition(row,column,word)
    invalid_condition(word,column)

    if invalid_condition(word,column) == True:
     for i in range(column,len(word) + column):
        if arr_cross[row][i] == word[index] or arr_cross[row][i] == "*":
            index += 1
        else:
            return False
   
    new_index = 0
    for i in range(column,len(word) + column):
        arr_cross[row][i] = word[new_index]
        new_index += 1
    return True

