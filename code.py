import numpy as np
arr_cross = np.zeros((10, 10) , dtype=object) 
arr_cross [:] = "*"

def crossword_input(text: str):
    n = int(text[0])+1
    moves = []
    words = []
    words_1 = []

    for i in range(1, n): 
        moves.append(text[i])
    for i in range(n, len(text)):  
        words.append(text[i])
        words_1.append(text[i])

    words = words[::-1]
    words_1 = words_1[::-1]
    return moves, words, words_1

