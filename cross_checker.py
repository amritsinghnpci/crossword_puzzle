def crossword_checker(n: int,text: str) -> str:
    moves, words, words_1 = crossword_input(text)

    for i in range(len(words)):
        for j in range(len(moves)):
            row = moves[j][0]
            column = moves[j][1]
        
            if moves[j][2] == "D" and down_crossword(int(row), int(column), words_1[i]) == True: 
                words.remove(words_1[i])
                break
            if moves[j][2] == "A" and across_crossword(int(row), int(column), words_1[i]) == True: 
                words.remove(words_1[i])
                break
    
    if len(words) > 1 or len(words[0]) < 3:
        return f"Trial {n}: Impossible"
    return f"Trial {n}: {words[0]}"
